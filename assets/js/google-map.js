function initMap() {
  var location = {lat: 14.676030, lng: 120.982422};
  var map = new google.maps.Map(document.getElementById('map'), {zoom: 15, center: location});
  var marker = new google.maps.Marker({position: location, map: map});
}